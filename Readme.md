
## what this

配置文件语法相对简单，当前仅为自己使用的 make 系统。
识别配置文件，然后生成 build.ninja 文件；
实际的 build 过程是通过 ninja 来完成。

当前考虑仅仅支持 clang，lld， 目标平台是 cortex，risc-v 嵌入式开发应用。


## parser

借用 golang 标准库中自带的词法分析， go/scanner 。
需要稍微修正一些 keyword ，把他们变成 ident。


## make logical

形式上使用 struct 定义语法。
但是构造逻辑关系，参考 scons 的组成结构。

File
Object
Program

三个层次，第一个 File 不会生成 ninja 的 build 向量。
后面两个 Objcet， Progam 会清晰的生成对应的 build 向量。

具体内容请查看 [详细参考](./docs/manual.md)


## 节点组成

node 通过 interface 对行为标准化。
1. 输出是一个数组，数组成员是 file 对象 (文件路径是必须的, 考虑是否输出文件类型 )
2. 输入是一个数组，数组成员也是 node。
3. 每个 node 提供一个 build 函数接口，通过 ninja 对象输出若干 build edge。
4. 最终输出过程，对这个树状结构深度遍历，从最底层叶子节点，执行 build 动作。


## TODO

1. 支持编译过程中生成 depend 文件.
2. 支持定制 objcopy 类型的文件处理动作, 单输入, 单输出?
3. 支持 submodule 引用, 把子目录层次化集成在一起.
4. 特性 submodule 功能实现之后, 支持变量传递, 目录名字传递?








