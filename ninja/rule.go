package ninja

import "strings"

type Rule struct {
	key     string
	command string
	descrip string
}

func NewRule(key string, cmd string) *Rule {
	var ru Rule

	/**/
	ru.key = key
	ru.command = cmd
	ru.descrip = ""

	return &ru
}

func (ru *Rule) String() string {
	var b strings.Builder

	/**/
	b.WriteString("rule " + ru.key + "\n")
	b.WriteString("  command = " + ru.command + "\n")
	if len(ru.descrip) > 0 {
		b.WriteString("  description = " + ru.descrip + "\n")
	}

	/**/
	return b.String()
}

func (ru *Rule) SetDesc(desc string) {
	ru.descrip = desc
}
