package ninja

type Variable struct {
	key string
	val string
}

func NewVariable(key string, val string) *Variable {
	var vb Variable

	/**/
	vb.key = key
	vb.val = val

	/**/
	return &vb
}

func (vb *Variable) String() string {
	return vb.key + " = " + vb.val + "\n"
}

func (vb *Variable) AddStr(ass string) {
	vb.val = vb.val + " " + ass
}
