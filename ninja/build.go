package ninja

import "strings"

type Build struct {
	target []string
	source []string
	rule   string
	varibs []*Variable
}

func NewBuild(tar []string, src []string, ru string) *Build {
	var bd Build

	/**/
	bd.target = tar
	bd.source = src
	bd.rule = ru

	/**/
	return &bd
}

func (bd *Build) AddVar(key string, val string) {
	vb := NewVariable(key, val)
	bd.varibs = append(bd.varibs, vb)
	return
}

func (bd *Build) String() string {
	var b strings.Builder

	b.WriteString("build " + strings.Join(bd.target, " ") + " : ")
	b.WriteString(bd.rule + " " + strings.Join(bd.source, " ") + "\n")

	for _, vb := range bd.varibs {
		b.WriteString("  " + vb.String())
	}

	return b.String()
}
