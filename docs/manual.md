
# 基本类型


# File 结构定义

   type : string : 目前支持 "c" 或者 "asm"
srcpath : string : 搜索源代码文件的路径名.
pattern : array of string : 通配符来过滤若干 或者 文件全名.


# Object 结构定义

objpath : string : 存放 obj 文件的路径.
 source : array of File : 若干个源代码文件, file 对象.
cpppath : array of string : 编译时增加的 -I 若干目录.
 ccopts : array of string : 编译时增加的其他参数.


# Program 结构定义

target : string : 单个目标文件名字.
source : array of Object : 若干 object 对象.
script : string : 指定的 link script 文件名字.


# Binary 结构定义

target : string : 单个目标文件名字.
source : program : 单个 program 对象

